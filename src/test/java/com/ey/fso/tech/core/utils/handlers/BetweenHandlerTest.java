package com.ey.fso.tech.core.utils.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;
import com.ey.fso.tech.core.utils.date.operations.handlers.BetweenHandler;
import com.ey.fso.tech.core.utils.date.operations.model.DateValueFormat;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.text.ParseException;

public class BetweenHandlerTest {
    private static final String validDateFormat = "mm/DD/yyyy";
    private static final String invalidDateFormat = "m/dd-yyyy";

    DateValueFormat d1;
    DateValueFormat d2;
    DateValueFormat d3;
    DateValueFormat d4;
    DateValueFormat d5;
    DateValueFormat d6;

    @Before
    public void initData(){
        d1 = new DateValueFormat();
        d1.setFormat(invalidDateFormat);
        d1.setValue("01/01/2018");

        d3 = new DateValueFormat();
        d3.setFormat(invalidDateFormat);
        d3.setValue("01/01/201");

        d3 = new DateValueFormat();
        d3.setFormat(invalidDateFormat);
        d3.setValue("01/01/2018");

        d5 = new DateValueFormat();
        d5.setFormat(invalidDateFormat);
        d5.setValue("01/01/2020");

        d2 = new DateValueFormat();
        d2.setFormat(validDateFormat);
        d2.setValue("1/1/2017");

        d4 = new DateValueFormat();
        d4.setFormat(validDateFormat);
        d4.setValue("01/01/2019");

        d6 = new DateValueFormat();
        d6.setFormat(validDateFormat);
        d6.setValue("01/01/2021");
    }
    @Test
    public void testTrue() throws ParseException, IllegalArgumentException {
        AbstractDateOperationHandler betweenHandler = new BetweenHandler();
        boolean result = betweenHandler.evaluate(d4, d2, d6);
        assertTrue(result);
    }

    @Test
    public void testFalse() throws ParseException, IllegalArgumentException {
        AbstractDateOperationHandler betweenHandler = new BetweenHandler();
        boolean result = betweenHandler.evaluate(d2, d4, d6);
        assertFalse(result);
    }

    @Test(expected = ParseException.class)
    public void testInvalidDateFormat() throws ParseException, IllegalArgumentException {
        AbstractDateOperationHandler betweenHandler = new BetweenHandler();
        betweenHandler.evaluate(d1, d3, d5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidArgument() throws ParseException, IllegalArgumentException{
        AbstractDateOperationHandler betweenHandler = new BetweenHandler();
        betweenHandler.evaluate(null, d1, d2);
    }

}
