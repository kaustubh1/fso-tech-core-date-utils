package com.ey.fso.tech.core.utils.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;
import com.ey.fso.tech.core.utils.date.operations.handlers.EqualsHandler;
import com.ey.fso.tech.core.utils.date.operations.model.DateValueFormat;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.text.ParseException;

public class EqualsHandlerTest {

    private static final String validDateFormat = "MM/dd/yyyy";
    private static final String invalidDateFormat = "m/dd-yyyy";
    private  static  final String validDateFormat2 = "yyyy-MM-dd'T'HH:mm:ss.SSSX";

    DateValueFormat d1;
    DateValueFormat d2;
    DateValueFormat d3;
    DateValueFormat d4;
    DateValueFormat d5;
    DateValueFormat d6;

    @Before
    public void initData(){
        d1 = new DateValueFormat();
        d1.setFormat(invalidDateFormat);
        d1.setValue("01/01/2018");

        d3 = new DateValueFormat();
        d3.setFormat(invalidDateFormat);
        d3.setValue("01/01/201");

        d3 = new DateValueFormat();
        d3.setFormat(invalidDateFormat);
        d3.setValue("01/01/2018");

        d5 = new DateValueFormat();
        d5.setFormat(invalidDateFormat);
        d5.setValue("01/01/2020");

        d2 = new DateValueFormat();
        d2.setFormat(validDateFormat);
        d2.setValue("01/01/2018");

        d4 = new DateValueFormat();
        d4.setFormat(validDateFormat);
        d4.setValue("04/23/2018");

        d6 = new DateValueFormat();
        d6.setFormat(validDateFormat2);
        d6.setValue("2018-04-23T00:00:00.000Z");
    }

    @Test
    public void testEqualityTrue() throws ParseException, IllegalArgumentException {
        AbstractDateOperationHandler equalsHandler = new EqualsHandler();
        boolean result = equalsHandler.evaluate(d2, d2);
        assertTrue(result);
    }

    @Test
    public void testEqualityFalse() throws ParseException, IllegalArgumentException {
        AbstractDateOperationHandler equalsHandler = new EqualsHandler();
        boolean result = equalsHandler.evaluate(d2, d4);
        assertFalse(result);
    }

    @Test(expected = ParseException.class)
    public void testInvalidDateFormat() throws ParseException, IllegalArgumentException {
        AbstractDateOperationHandler betweenHandler = new EqualsHandler();
        betweenHandler.evaluate(d1, d3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidArgument() throws ParseException, IllegalArgumentException{
        AbstractDateOperationHandler betweenHandler = new EqualsHandler();
        betweenHandler.evaluate(null, d1);
    }

    @Test
    public void testDifferentDateFormats() throws ParseException {
        AbstractDateOperationHandler equalsHandler = new EqualsHandler();
        assertTrue(equalsHandler.evaluate(d6,d4));

    }

}
