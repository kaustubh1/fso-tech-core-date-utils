package com.ey.fso.tech.core.utils.date.operations.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;

import javax.naming.ldap.UnsolicitedNotification;
import java.util.Date;

/**
 * @author Kaustubh.Kaustubh
 */
public class GreaterThanEqualHandler extends AbstractDateOperationHandler {

    @Override
    public boolean evaluateInternal(Date dateToCompare, Date referenceDate) {
        return new EqualsHandler().evaluateInternal(dateToCompare, referenceDate) ||
                new GreaterThanHandler().evaluateInternal(dateToCompare, referenceDate);
    }

    public boolean evaluateInternal(Date dateToCompare, Date... dates) {
        throw new UnsupportedOperationException();
    }
}
