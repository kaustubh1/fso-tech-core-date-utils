package com.ey.fso.tech.core.utils.date.operations.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;

import java.util.Date;

/**
 * mulaysh
 * 7/29/18
 */
public class LessThanEqualsHandler extends AbstractDateOperationHandler {

    @Override
    public boolean evaluateInternal (Date dateToCompare, Date referenceDate){
        return ((new EqualsHandler()).evaluateInternal(dateToCompare, referenceDate) ||
                (new LessThanHandler()).evaluateInternal(dateToCompare, referenceDate));
    }

    public boolean evaluateInternal(Date dateToCompare, Date... dates) {
        throw new UnsupportedOperationException();
    }
}
