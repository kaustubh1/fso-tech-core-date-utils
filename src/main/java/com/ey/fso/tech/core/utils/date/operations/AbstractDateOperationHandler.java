package com.ey.fso.tech.core.utils.date.operations;

import com.ey.fso.tech.core.utils.date.operations.model.DateValueFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author mulaysh
 * 7/29/18
 * @author Kaustubh.Kaustubh
 */
public abstract class AbstractDateOperationHandler {

    private static Date getDate(DateValueFormat dateValueFormat) throws ParseException {
        SimpleDateFormat dateFormatData = new SimpleDateFormat(dateValueFormat.getFormat());
        dateFormatData.setLenient(false);
        Date date = dateFormatData.parse(dateValueFormat.getValue());
        return date;
    }

    private static void validateArguments(DateValueFormat dateToCompare, DateValueFormat... referenceDates)
            throws IllegalArgumentException{
        if(dateToCompare == null && referenceDates == null)
            throw new IllegalArgumentException("dateToCompare and referenceDates arguments cannot be null");
        if(dateToCompare == null)
            throw new IllegalArgumentException("dateToCompare argument cannot be null");
        else if(referenceDates == null)
            throw new IllegalArgumentException("referenceDate argument cannot be null");
    }

    public static void validateDates(DateValueFormat dateToCompare, DateValueFormat referenceDate)
            throws ParseException {
        if(!referenceDate.getFormat().equalsIgnoreCase(dateToCompare.getFormat())){
            Date toBeCompared = getDate(dateToCompare);
            SimpleDateFormat sfd = new SimpleDateFormat(referenceDate.getFormat());
            dateToCompare.setValue(sfd.format(toBeCompared));
            dateToCompare.setFormat(referenceDate.getFormat());
        }
    }

    public boolean evaluate(DateValueFormat dateToCompare, DateValueFormat referenceDate)
            throws ParseException {
        AbstractDateOperationHandler.validateArguments(dateToCompare, referenceDate);
        AbstractDateOperationHandler.validateDates(dateToCompare, referenceDate);
        Date dtCompare = AbstractDateOperationHandler.getDate(dateToCompare);
        Date refDate = AbstractDateOperationHandler.getDate(referenceDate);
        return evaluateInternal(dtCompare, refDate);
    }



    public boolean evaluate(DateValueFormat dateToCompare, DateValueFormat... referenceDates)
            throws ParseException {

        AbstractDateOperationHandler.validateArguments(dateToCompare, referenceDates);
        Date dtCompare = AbstractDateOperationHandler.getDate(dateToCompare);
        List<Date> dates = new ArrayList<Date>();
        for (DateValueFormat referenceDate : referenceDates) {
            Date date = AbstractDateOperationHandler.getDate(referenceDate);
            dates.add(date);
        }

        return evaluateInternal(dtCompare, dates.toArray(new Date[0]));
    }

    public abstract boolean evaluateInternal(Date dateToCompare, Date referenceDate);

    public abstract boolean evaluateInternal(Date dateToCompare, Date... dates);

}
