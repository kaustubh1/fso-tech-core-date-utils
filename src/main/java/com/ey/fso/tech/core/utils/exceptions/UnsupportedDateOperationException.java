package com.ey.fso.tech.core.utils.exceptions;

/**
 * mulaysh
 * 7/29/18
 */
public class UnsupportedDateOperationException extends Exception {
    public UnsupportedDateOperationException() {
        super();
    }

    public UnsupportedDateOperationException(String message) {
        super(message);
    }
}
