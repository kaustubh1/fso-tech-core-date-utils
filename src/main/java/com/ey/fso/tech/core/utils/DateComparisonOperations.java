package com.ey.fso.tech.core.utils;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;
import com.ey.fso.tech.core.utils.date.operations.handlers.*;

/**
 * mulaysh
 * 7/29/18
 */
public enum DateComparisonOperations {

    EQUALS ("=", new EqualsHandler()),
    LESS_THAN("<", new LessThanHandler()),
    LESS_THAN_EQUALS("<=", new LessThanEqualsHandler()),
    GREATER_THAN(">", new GreaterThanHandler()),
    GREATER_THAN_EQUALS(">=", new GreaterThanEqualHandler()),
    BETWEEN("><", new BetweenHandler()),
    IN("IN", new InHandler());

    private final String operator;
    private final AbstractDateOperationHandler handler;

    // enum definition
    DateComparisonOperations(String operator, AbstractDateOperationHandler handler){
        this.operator = operator;
        this.handler = handler;
    }

    public String getOperator() {
        return operator;
    }

    public AbstractDateOperationHandler getHandler() {
        return handler;
    }


    /*
        Could have used the Optional here, instead of returning null, but might be
        an overkill in this case as usually the Optional are not very performant
        compared to null checks directly.
        TODO: Someone should research this.
     */
    public static AbstractDateOperationHandler getHandler(String operationToCompare){
        for(DateComparisonOperations operation : DateComparisonOperations.values()){
            if (operation.getOperator().equals(operationToCompare)){
                return operation.handler;
            }
        }
        return null;
    }
}
