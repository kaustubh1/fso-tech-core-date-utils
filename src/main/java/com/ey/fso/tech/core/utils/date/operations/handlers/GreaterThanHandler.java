package com.ey.fso.tech.core.utils.date.operations.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;

import java.util.Date;

/**
 * @author Kaustubh.Kaustubh
 */
public class GreaterThanHandler extends AbstractDateOperationHandler {

    @Override
    public boolean evaluateInternal(Date dateToCompare, Date referenceDate) {
        return dateToCompare.after(referenceDate);
    }

    public boolean evaluateInternal(Date dateToCompare, Date... dates) {
        throw new UnsupportedOperationException();
    }
}
