package com.ey.fso.tech.core.utils.date.operations.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;

import java.util.Date;

public class WithinHandler extends AbstractDateOperationHandler {
    public boolean evaluateInternal(Date dateToCompare, Date referenceDate) {
        return false;
    }

    public boolean evaluateInternal(Date dateToCompare, Date... dates) {
        return false;
    }
}
