package com.ey.fso.tech.core.utils;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;
import com.ey.fso.tech.core.utils.date.operations.model.DateValueFormat;
import com.ey.fso.tech.core.utils.exceptions.UnsupportedDateOperationException;

import java.text.ParseException;

/**
 * @author mulaysh
 * 7/29/18
 * Support "<, >, <=, >=, = comparison operations"
 */
public class CoreDateUtils {
    private static final CoreDateUtils INSTANCE = new CoreDateUtils();

    private CoreDateUtils(){

    }
    /**
     * API method for binary operation calls like "<", ">", etc.
     * @param operation operator like "<"
     * @param dateToCompare
     * @param referenceDate
     * @return boolean result of comparison operation
     * @throws UnsupportedDateOperationException when number of operands does not match
     * expected number of operands
     * @throws ParseException
     */
    public static boolean compareDates(String operation
            , final DateValueFormat dateToCompare
            , final DateValueFormat referenceDate) throws UnsupportedDateOperationException, ParseException{
        AbstractDateOperationHandler handler = DateComparisonOperations.getHandler(operation);
        if (handler == null) {
            throw new UnsupportedDateOperationException();
        }
        else{
            return handler.evaluate(dateToCompare, referenceDate);
        }
    }


    /**
     *
     * @param operation
     * @param dateToCompare
     * @param dateRange reference date(s) the compare date has to be compared with.
     *
     * @return boolean result of comparison
     * @throws UnsupportedDateOperationException when number of operand do not match
     * expected number of operands for a particular operator
     * @throws ParseException
     */
    public static boolean compareDatesInRange(String operation
            , DateValueFormat dateToCompare
            , DateValueFormat... dateRange)
            throws UnsupportedDateOperationException, ParseException {

        AbstractDateOperationHandler handler = DateComparisonOperations.getHandler(operation);
        if(handler == null){
            throw new UnsupportedDateOperationException();
        }
        else{
            return handler.evaluate(dateToCompare, dateRange);
        }
    }

}
