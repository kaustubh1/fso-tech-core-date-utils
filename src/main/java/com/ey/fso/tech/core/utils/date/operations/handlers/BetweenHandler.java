package com.ey.fso.tech.core.utils.date.operations.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;

import java.util.Date;

/**
 * @author Kaustubh.Kaustubh
 */
public class BetweenHandler extends AbstractDateOperationHandler {

    public boolean evaluateInternal(Date dateToCompare, Date referenceDate) {
        throw new UnsupportedOperationException();
    }

    public boolean evaluateInternal(Date dateToCompare, Date... dates) {
        Date startLimit = dates[0];
        Date endLimit = dates[1];

        boolean greaterThan = new GreaterThanHandler().evaluateInternal(dateToCompare, startLimit);
        boolean lesserThan = new LessThanHandler().evaluateInternal(dateToCompare, endLimit);
        if(greaterThan && lesserThan)
            return true;

        return false;
    }
}
