package com.ey.fso.tech.core.utils.date.operations.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;

import java.util.Date;

public class InHandler extends AbstractDateOperationHandler {
    public boolean evaluateInternal(Date dateToCompare, Date referenceDate) {
        throw new UnsupportedOperationException("In operation operates on more than two operands.");
    }

    public boolean evaluateInternal(Date dateToCompare, Date... dates) {
        for(Date date:dates){
            if(date.equals(dateToCompare))
                return true;
        }
        return false;
    }
}
