package com.ey.fso.tech.core.utils.date.operations.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class DateValueFormat implements Serializable {
    private String value;
    private String format;

    public DateValueFormat(){}

    public DateValueFormat(String value, String format) {
        this.value = value;
        this.format = format;
    }
}
