package com.ey.fso.tech.core.utils.date.operations.handlers;

import com.ey.fso.tech.core.utils.date.operations.AbstractDateOperationHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * mulaysh
 * 7/29/18
 */
public class EqualsHandler extends AbstractDateOperationHandler {

    @Override
    public boolean evaluateInternal (Date dateToCompare, Date referenceDate){
            if (dateToCompare.equals(referenceDate)) {
                return Boolean.TRUE;
            }
        return false;
    }

    public boolean evaluateInternal(Date dateToCompare, Date... dates) {
        throw new UnsupportedOperationException();
    }
}
